const express = require('express');
const serveStatic = require('serve-static');
const path = require('path')

app = express();
app.use(serveStatic(__dirname + "/dist"));
const port = process.env.PORT || 5555;


app.get('*', (req,res)=>{
  res.sendFile(path.join(__dirname, './dist', 'index.html'));
})

app.listen(port, () => {
   console.log(`Server running at http://localhost:${port}/`);
 });